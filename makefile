# CSE 450 Project 3
# Makefile

simplecalc:	simplecalc.o y.tab.o lex.yy.o Node.o functions.h
	g++ simplecalc.o Node.o y.tab.o lex.yy.o  -o simplecalc -g

Node.o: Node.cc Node.h
	g++ -c Node.cc -g

lex.yy.c: simplecalc.l y.tab.h
	flex simplecalc.l

lex.yy.o: lex.yy.c
	gcc -c lex.yy.c -g

y.tab.c y.tab.h:  simplecalc.y
	bison --yacc --defines simplecalc.y

y.tab.o: y.tab.c y.tab.h
	gcc -c y.tab.c -g

simplecalc.o: simplecalc.cc y.tab.h
	g++ -c simplecalc.cc -g

test:
	./simplecalc < test1
	./simplecalc < test2
	./simplecalc < test3

clean:
	rm -f *.o simplecalc lex.yy* y.tab*
