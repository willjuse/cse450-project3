/*
 * CSE 450 Project 3
 * Group Members:
 * Matthew Vorce - vorcemat
 * Andrew Matteson - mattes18
 * William Juszczyk - juszczy1
 *
 * C++ file for expression calculator
 */


#include <cstdio>
#include <iostream>
#include <list>
#include "Node.h"
#include "functions.h"
#include <cstring>

using namespace std;
CNode *g_root = NULL;

vector<CNode*> rootlist;

extern "C" void setRoot(void *a)
{
	g_root = (CNode *)a;
}

extern "C" void Execute()
{
	if(g_root != NULL) {
		g_root->Evaluate();
	}
}

// get the double value of a node
extern "C" const char * getDoubleValueAsString(CNode * node)
{
	char * mystr = new char[16];
	mystr[0]='\0';
	sprintf(mystr, "%lg", node->Evaluate().DVal());
	return mystr;
}

extern "C" int rootEvaluatesToNonzero()
{
	//printf("eval: %d\n", (int)(g_root->Evaluate().DVal()));
	return (int)(g_root->Evaluate().DVal()) > 0;
}

extern "C" void clearExecutionList()
{
	rootlist.clear();
}

extern "C" void addToExecutionList(CNode * node)
{
	rootlist.push_back(node);
}

extern "C" void executeList()
{
	//printf("Executing the list...\n");
	for (vector<CNode*>::iterator iter = rootlist.begin();
		iter != rootlist.end();
		++iter)
	{
		//sprintf("Executing one element of the list...\n");
//		((CNode*)createNodePrint(((void*)*iter)))->Evaluate();
		//(new CNodePrint(*iter))->Evaluate();
		(*iter)->Evaluate();
	}
}

extern "C" {
#include "y.tab.h"
    extern FILE *yyin;
    extern int yylex();
    extern int yyparse();
    extern char *yytext;
}

int main(int argc, char **argv)
{
    FILE *file;
    
    if(argc < 2)
    {
        fprintf(stderr, "Using Standard input: Use ^D to exit.\n");
        file = stdin; 
    } else {
        file = fopen(argv[1], "r");
    }
    
    if(!file)
    {
        fprintf(stderr, "Unable to open file %s", argv[1]);
        return 1;
    }
    
    //cout << "Simple 2.0!" << endl;
    
    yyin = file;
    
    do
    {
        yyparse();
    } while(!feof(yyin));
    
    return 0;
}

// Vectors for storing variables
vector<double> values;
vector<string> variables;

extern "C" bool CheckVariable(char *s, double *value)
{
	string var(s);
	for(int i = 0; i < variables.size(); i++)
	{
		if(variables[i] == var)
		{
			(*value) = values[i];
			return true;
		}
	
	}
	
	return false;
}

extern "C" void AddNewVariable(char *s, double value)
{
	string var(s);
	for(int i = 0; i < variables.size(); i++)
	{
		if(variables[i] == var)
		{
			variables[i] = var;
			values[i] = value;
			return;
		}
	}
	variables.push_back(var);
	values.push_back(value);
}

extern "C" void yyerror(char *s)
{ 
    cout << s << endl;
}

extern "C" void answer(double n)
{
	cout << "Answer:  " << n << endl;
}

extern "C" void print(char *s)
{
    cout << s << endl;
}
