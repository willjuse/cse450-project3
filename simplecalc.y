%{
/*
 * CSE 450 Project 3
 * Group Members:
 * Matthew Vorce - vorcemat
 * Andrew Matteson - mattes18
 * William Juszczyk - juszczy1
 *
 * Expression evaluator bison file
 */
#include "stdio.h"
#include "functions.h"
#include "math.h"

extern int nestlevel;
// if 0, means we're *outside* of an IF, and means we should
// evaluate statements as soon as they're entered

void answer(int n);
%}

%union {
	int	intval;
	double	dval;
	char *  sval;
	void *pval;		// Pointer to tree nodes
}

%token<dval> NUM
%token<sval> STRING
%token<sval> POSNEWVAR
%token ERROR
%token<sval> STRTOPRINT
%token<sval> PRINT
%token EMPTYPRINT
%token<sval> SQRT
%token<sval> IF
%token<sval> WHILE
%token<sval> COMMENT
%token<sval> KEYWORD
%token<sval> SUPPORT
%token<sval> COMMA
%token<pval> EXISTINGVAR

%type<pval> STRTOKEN CMDNODE SUPFUNC
%type<pval> REALNUM E T2 T1 S NEWVAR VAREXP ARG STATFUNC FUNCNAME COMMAND

%token LPAREN RPAREN LBRACE RBRACE

%%
F :     		F '\n' S
| 				S;

S :     EXP  	{
						if (nestlevel==0) {
							setRoot($<pval>1); // print expressions like 2+2, etc
							Execute();
						} else if (nestlevel==1) { 
							/*printf("adding statement to execution list..\n");*/
							addToExecutionList($<pval>1); 
						} /*answer($<dval>$);*/
					};

								
/*S :			COMMAND					{printf("\n"); /*print($<sval>1);};*/
S :			COMMENT {};
S :     FULLC               {/*printf("making if statement\n");*/ if(rootEvaluatesToNonzero()){executeList();}};
S :     FULLW               {/*printf("making while statement\n");*/ while(rootEvaluatesToNonzero()){executeList();}};
S :     {} ;
S :     ERROR { } ;

/* Potential expressions that get executed */
EXP:		E 				{$<pval>$ = createNodePrint($<pval>1);};
EXP:		STATFUNC		{};
EXP:		VAREXP		{};
EXP:		SUPFUNC		{};
EXP:		COMMAND		{};

FULLC : C '\n' LBRACE '\n' SLIST '\n' RBRACE {/*printf("full conditional\n");*/};
| C LBRACE '\n' SLIST 'n' RBRACE {/*printf("full conditional\n");*/};

FULLW : W '\n' LBRACE '\n' SLIST '\n' RBRACE {/*printf("full conditional\n");*/};
| W LBRACE '\n' SLIST '\n' RBRACE {/*printf("full conditional\n");*/};

SLIST:  SLIST '\n' S
				| S;

C :			IF LPAREN E RPAREN {/*printf("detected if statement\n"); */setRoot($<pval>3);};
W :			WHILE LPAREN E RPAREN {/*printf("detected while statement\n"); */setRoot($<pval>3);};

/*VAREXP: 		NEWVAR E					{/*AddNewVariable($<sval>1, $<dval>2); printf("New variable added\n");};*/

REALNUM: 	LPAREN E RPAREN 		{$$ = $2;}; /* evaluate expressions as soon as we can "(5+5)"-->"10" */
REALNUM: 	SQRT LPAREN E RPAREN	{$$ = createNodeSqrt($3);};

//
// Create expression which is a variable assignment
//

VAREXP :     NEWVAR E      			{ $$ = createNodeVariable($1, $2); /*setLastVarNodeUsed($$);*/ /*printf("Creating variable node:  %p %p\n", $1, $2); */};

//
// Statistics function support
//

STATFUNC:	FUNCNAME LPAREN ARGLIST RPAREN		{ $$ = createNodeFunction($1); /*printf("Creating stat function!\n");*/};

ARGLIST:		ARG						{addToNodeArgumentList($1);/* printf("Adding first argument to argument list!\n");*/};
|				ARGLIST COMMA ARG		{addToNodeArgumentList($3); /*printf("Adding argument to argument list!\n");*/};

ARG:			E						{};

FUNCNAME:	KEYWORD				{$$ = createNodeStr($<sval>1);}

//
// Support functions
//

SUPFUNC:		FUNCNAME				{$$ = createNodeFunction($1); /*printf("Creating support function!\n");*/}

FUNCNAME:	SUPPORT				{$$ = createNodeStr($<sval>1);}


/* T1 operators are more important (higher precedence) than T2 operators */

E :			E '+' T2        	{$$ = createNodeAddition($1, $3); /*printf("Creating addition node: %p %p\n", $1, $3);*/}
|    			E '-' T2        	{$$ = createNodeSubtraction($1, $3); }
|		 		E '<' T2				{$$ = createNodeLessThan($1, $3);}
|		 		E '>' T2				{$$ = createNodeGreaterThan($1, $3);};

E :			T2               	{$$ = $1;};

T2 :    		T2 '*' T1    {$$ = createNodeMultiplication($1, $3); } /* fix 5^2*5^2 and (5^2)*5^2 [should be T1, not REALNUM here] */
|       		T2 '/' T1    {$$ = createNodeDivision($1, $3); }
|			T1;

T1 :    		T1 '^' REALNUM 	{$$ = createNodePower($1, $3);}
|				REALNUM           {$$ = $1;};


REALNUM :	'-' NUM				{$$ = createNodeValue(-$<dval>2); /*$<dval>$ = -$<dval>2;*/}
| 				NUM					{/*printf("d: %lf\n", $<dval>1);*/ $$ = createNodeValue($<dval>1); /*$<dval>$ = $<dval>1;*/}
|       EXISTINGVAR  {/*printf("dynamic var: %s\n", $<sval>1);*/ $$ = createNodeValueFromVar($<sval>1, 0 /* no negate */);}
|       '-' EXISTINGVAR {/*printf("dynamic var: %s\n", $<sval>1);*/ $$ = createNodeValueFromVar($<sval>1, 1 /* negate */);};


/* Handles assigning into a variable */

NEWVAR:		POSNEWVAR			{$$ = createNodeStr($<sval>1); /*printf("Creating string node %s\n", $<sval>1); */};

/*COMMAND:  PRINT CMDNODE {$<sval>$ = $<sval>2; printf("%s", $<sval>2);}*/
/*CMDNODE: LITERALSTR ' ' LITERALSTR {printf("%s %s", $<sval>1, $<sval>3);}*/

COMMAND: 	PRINT CMDNODE { $<pval>$ = createNodePrint($<pval>2 /*CNodeStringList*/); }
|				PRINT { $<pval>$ = createNodePrint(createNodeStr("")); };

CMDNODE:	CMDNODE STRTOKEN { $<pval>$ = appendNodeToStringList($<pval>1 /*CNodeStringList*/, $<pval>2 /*CNode*/); }
| 				STRTOKEN { $<pval>$ = initializeStringListFromNode($<pval>1 /*CNode*/); };

//<<<<<<< HEAD
//STRTOKEN: 	STRTOPRINT { /*printf("%s", $<sval>1); printf("str: %s\n", $<sval>1);*/ $$ = $<sval>1; }
//|					E { $<sval>$ = getDoubleValueAsString($<pval>1); } /* causes shift/reduce error */
//=======
STRTOKEN: 	STRTOPRINT { /*printf("%s", $<sval>1);*/ /*printf("str: %s\n", $<sval>1);*/ $<pval>$ = createNodeStr($<sval>1); }
|					E { $<pval>$ = $<pval>1; } /* causes shift/reduce error */
//>>>>>>> ed1e4f14813754ee90161c71fe3bd8d66af6e370
|         NEWVAR {};

%%
