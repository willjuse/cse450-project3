#pragma once

class CNode;

extern CNode *g_root;

#include <iostream>
#include <cstdio>
#include <string>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <cstring>

extern "C" bool CheckVariable(char *s, double *value);

class Val
{
public:
	Val() {type = NONE;}
	Val(double i) {type = DOUBLE;  dval = i;}
	Val(std::string &s) {type = STR; /*printf("strlen: %d [%s]\n", strlen(s.c_str()), s.c_str());*/ sval = new std::string(s);}
	~Val() {if(type == STR) {delete sval;}}

	enum Type {NONE, DOUBLE, STR};
	
	Val(const Val &v)
	{
		if(v.GetType() == DOUBLE)
		{
			type = DOUBLE;
			dval = v.DVal();
		}
		else if(v.GetType() == STR)
		{
			//printf("v type is str");
			type = STR;
			sval = new std::string(v.SVal());
		}
		else
		{
			printf("Error: variable type is not double or str");
		}
	}

	Val &operator=(const Val &v)
	{
		if(type == STR) {delete sval;}

		if(v.GetType() == DOUBLE)
		{
			type = DOUBLE;
			dval = v.DVal();
		}
		else if(v.GetType() == STR)
		{
			type = STR;
			sval = new std::string(v.SVal());
		}
		return *this;
	}

	const char * StrRep()
	{
		if (type == STR) { return SVal().c_str(); }
		else if (type == DOUBLE) {
			char a[256];
			//a[0] = '\0';
			sprintf(a, "%lg", DVal());
			//a[strlen(a)] = '\0';
			return strdup(a);
		}
		//return strdup("HELLO");
	}

	void Print()
	{

		//if (type == STR) { printf("%s\n", SVal().c_str()); }
		//else if (type == DOUBLE) { printf("%lg", DVal()); }
		//if (type == STR) { printf("%s", SVal().c_str()); }
		//else if (type == DOUBLE) { printf("%lg", DVal()); }
		printf("%s", StrRep());
	}

	Type GetType() const {return type;}
	double DVal() const {return dval;}
	const std::string &SVal() const {return *sval;}

private:
	Type  type;
	union
	{
		std::string  *sval;
		double           dval;
	};
};


class CNode
{
public:
	CNode(void) {}
	virtual ~CNode(void) {}

	virtual Val Evaluate() {return Val();}
};


class CNodeValue : public CNode
{
public:
	CNodeValue(double i, bool neg) {using_var = false; negate = neg; val = i;}
	CNodeValue(const char * v, bool neg) {using_var = true; negate = neg; /*printf("set v: %s\n", v); */var_name = v;}

	virtual Val Evaluate() {
		if (using_var) {
				double gotten_value;
				/*printf("looking up %s\n", var_name.c_str());*/
				bool flag = CheckVariable(const_cast<char*>(var_name.c_str()), &gotten_value);
				if(flag)
				{
					if (negate)
						return Val(-gotten_value);
					else
						return Val(gotten_value);
				} else {
					// variable doesn't exist? return 0?
					printf("Error Variable has not been defined\n");
					return Val(0);
				}
    }
		else {
			if (negate)
				return Val(-val);
			else
				return Val(val);
		}
	}

private:
  bool using_var;
  bool negate;
  std::string var_name;
	double val;
};




class CNodePrint : public CNode
{
public:
	CNodePrint(CNode *a) {node = a;}

	virtual Val Evaluate() {node->Evaluate().Print(); printf("\n"); return Val();}

private:
	CNode *node;
};




class CNodeStr : public CNode
{
public:
	CNodeStr(const char *s) {val = s;}

	virtual Val Evaluate() {return Val(val);}

private:
	std::string val;
};

class CNodeAddition : public CNode
{
public:
	CNodeAddition(CNode *a, CNode *b) {pa = a;  pb = b;
		/*printf("add %lf + %lf\n", pa->Evaluate().DVal(), pb->Evaluate().DVal());*/}

	virtual Val Evaluate()
	{
		return Val(pa->Evaluate().DVal() + pb->Evaluate().DVal());
	}

private:
	CNode *pa;
	CNode *pb;
};

class CNodeSubtraction : public CNode
{
public:
	CNodeSubtraction(CNode *a, CNode *b) {pa = a;  pb = b;
		/*printf("sub %lf - %lf\n", pa->Evaluate().DVal(), pb->Evaluate().DVal());*/}

	virtual Val Evaluate()
	{
		return Val(pa->Evaluate().DVal() - pb->Evaluate().DVal());
	}

private:
	CNode *pa;
	CNode *pb;
};

class CNodeMultiplication : public CNode
{
public:
	CNodeMultiplication(CNode *a, CNode *b) {pa = a;  pb = b;
		/*printf("mul %lf * %lf\n", pa->Evaluate().DVal(), pb->Evaluate().DVal());*/}

	virtual Val Evaluate()
	{
		return Val(pa->Evaluate().DVal() * pb->Evaluate().DVal());
	}

private:
	CNode *pa;
	CNode *pb;
};

class CNodeDivision : public CNode
{
public:
	CNodeDivision(CNode *a, CNode *b) {pa = a;  pb = b;
		/*printf("div %lf / %lf\n", pa->Evaluate().DVal(), pb->Evaluate().DVal());*/}

	virtual Val Evaluate()
	{
		return Val(pa->Evaluate().DVal() / pb->Evaluate().DVal());
	}

private:
	CNode *pa;
	CNode *pb;
};

class CNodePower : public CNode
{
public:
	CNodePower(CNode *a, CNode *b) {pa = a;  pb = b;
		/*printf("pow %lf ^ %lf\n", pa->Evaluate().DVal(), pb->Evaluate().DVal());*/}

	virtual Val Evaluate()
	{
		return Val(pow(pa->Evaluate().DVal(), pb->Evaluate().DVal()));
	}

private:
	CNode *pa;
	CNode *pb;
};

class CNodeLessThan : public CNode
{
public:
	CNodeLessThan(CNode *a, CNode *b) {pa = a;  pb = b;
		/*printf("lt %lf < %lf\n", pa->Evaluate().DVal(), pb->Evaluate().DVal());*/}

	virtual Val Evaluate()
	{
		return Val((int)((pa->Evaluate().DVal() < pb->Evaluate().DVal())));
	}

private:
	CNode *pa;
	CNode *pb;
};

class CNodeGreaterThan : public CNode
{
public:
	CNodeGreaterThan(CNode *a, CNode *b) {pa = a;  pb = b;
		/*printf("gt %lf > %lf\n", pa->Evaluate().DVal(), pb->Evaluate().DVal());*/}

	virtual Val Evaluate()
	{
		return Val((int)((pa->Evaluate().DVal() > pb->Evaluate().DVal())));
	}

private:
	CNode *pa;
	CNode *pb;
};


class CNodeSqrt : public CNode
{
public:
	CNodeSqrt(CNode *a) {pa = a;
		/*printf("sqrt %lf\n", pa->Evaluate().DVal());*/}

	virtual Val Evaluate()
	{
		return Val(sqrt(pa->Evaluate().DVal()));
	}

private:
	CNode *pa;
};

class CNodeStmts : public CNode
{
public:
	CNodeStmts(CNode *a, CNode *b) {pa = a;  pb = b;}
	virtual ~CNodeStmts() {delete pa;  delete pb;}

	virtual Val Evaluate()
	{
		pa->Evaluate();
		pb->Evaluate();

		return Val();
	}

private:
	CNode *pa;
	CNode *pb;
};

// Vectors for storing variables
extern std::vector<double> values;
extern std::vector<std::string> variables;

class CNodeVariable : public CNode
{
public:
	CNodeVariable(CNode *a, CNode *b) {variable = a;  value = b; /*printf("Creating new variable\n");*/}
	virtual ~CNodeVariable() {delete variable;  delete value;}

	virtual Val Evaluate()
	{
		//printf("Evaluating a variable\n");
		// Add the new variable
		std::string var = variable->Evaluate().SVal();
		double val = value->Evaluate().DVal();
		//printf("Variable is %s\n", var);
		//std::string var = "";
		//double val = 0;
		//std::cout << "Variable is " << var << '\n';
		//printf("value is %f\n", val);
		for(int i = 0; i < variables.size(); i++)
		{
			//printf("i = %d\n", i);
			if(variables[i] == var)
			{
				variables[i] = var;
				values[i] = val;
				return Val();
			}
		}
		variables.push_back(var);
		values.push_back(val);
		//pa->Evaluate();
		//pb->Evaluate();

		return Val();
	}

private:
	CNode *variable;
	CNode *value;
};

//
// Handes statistics functions
//

class CNodeFunction : public CNode
{
public:
	CNodeFunction(CNode *a, std::vector<CNode *> b) {functionName = a;  arguments = b; /*printf("Creating new function\n");*/}
	virtual ~CNodeFunction() {delete functionName;}

	virtual Val Evaluate()
	{
		//printf("Evaluating a function\n");
		
		std::string funcName = functionName->Evaluate().SVal();
		
		double dAnswer = 0;
		std::string sAnswer = "";
		
		if(funcName == "mean" | funcName == "stdev" | funcName == "add")
		{
			dAnswer = EvaluateStatFunc(funcName);
			printf("%f\n", dAnswer);
			return Val(dAnswer);
		}
		else if(funcName == "help" | funcName == "exit" | funcName == "clear")
		{
			EvaluateSupportFunc(funcName);
			return Val(sAnswer);
		}

		return Val(dAnswer);
	}
	
	void EvaluateSupportFunc(std::string funcName)
	{
		if(funcName == "help")
		{
			printf("This is a simple calculator!\nThis calculator can do many types of calculations\n+, -, *, ^, <, >, mean, stdev, add, print, if and while\n\n");
		}
		else if(funcName == "exit")
		{
			exit(0);
		}
		else if(funcName == "clear")
		{
			//message = "Variables have been cleared!\n";
			printf("Variables have been cleared!\n");
			variables.clear();
			values.clear();
		}
		
	}
	
	double EvaluateStatFunc(std::string funcName)
	{
		double runningTotal = 0;
		int numOfElements = arguments.size();
		double answer = 0;
		
		// Add up all the arguments
		for(int i = 0; i < numOfElements; ++i)
		{
			runningTotal += arguments[i]->Evaluate().DVal();
		}
			
		if(funcName == "mean")
		{
			answer = runningTotal / numOfElements;
		}
		else if(funcName == "stdev")
		{
			double deviationTotal = 0;
			double mean = runningTotal / numOfElements;
			// Calculate the squared deviations
			for(int i = 0; i < numOfElements; ++i)
			{
				double deviation = (arguments[i]->Evaluate().DVal() - mean);
				deviationTotal += (deviation * deviation);
			}
			
			deviationTotal /= (numOfElements - 1);
			answer = sqrt(deviationTotal);
		}
		else if(funcName == "add")
		{
			answer = runningTotal;
		}
		
		//printf("Answer is : %f\n", answer);
		return answer;
	}

private:
	CNode *functionName;
	std::vector<CNode *> arguments;
};

class CNodeStringList : public CNode
{
public:
	void AddItem(CNode *s) { strlist.push_back(s); }
	CNodeStringList(CNode *s) { AddItem(s); }

	virtual Val Evaluate() {
		std::string str = "";
		//int i=0;
		for (std::vector<CNode*>::iterator iter = strlist.begin();
			iter != strlist.end();
			++iter) {
			//char a[256];
			//sprintf(a, "ITEM %d", i);
			//i++;
			//str += std::string("(") + std::string(a) + std::string(")") + (*iter)->Evaluate().StrRep();
			str += (*iter)->Evaluate().StrRep();
		};
		return Val(str);
	}

private:
	std::vector<CNode *> strlist;
};
