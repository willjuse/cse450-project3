#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include <stdbool.h>

void *createNodeValue(double n);
void *createNodeValueFromVar(const char * v, bool neg);
void *createNodePrint(void *a);
void *createNodeStr(const char *s);
void *createNodeAddition(void *a, void *b);
void *createNodeSubtraction(void *a, void *b);
void *createNodeMultiplication(void *a, void *b);
void *createNodeDivision(void *a, void *b);
void *createNodePower(void *a, void *b);
void *createNodeLessThan(void *a, void *b);
void *createNodeGreaterThan(void *a, void *b);
void *createNodeSqrt(void *a);
void *createNodeStmts(void *a, void *b);
void *createNodeVariable(void *a, void *b);

// Stat function stuff
void addToNodeArgumentList(void *a);
void *createNodeFunction(void *a);
#endif
