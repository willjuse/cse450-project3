extern "C" {
#include "functions.h"
}

#include "Node.h"
using namespace std;

vector<CNode *> arguments;
vector<CNode *> empty;

extern "C" void *createNodeValue(double n)
{
	CNodeValue *node = new CNodeValue(n, false /* no negation */);
	return node;
}

extern "C" void *createNodeValueFromVar(const char *varname, bool negate)
{
	CNodeValue *node = new CNodeValue(varname, negate);
	return node;
}

extern "C" void *createNodePrint(void *a)
{
	CNodePrint *node = new CNodePrint((CNode *)a);
	return node;
}

extern "C" void *createNodeStr(const char *s)
{
	CNodeStr *node = new CNodeStr(s);
	return node;
}

extern "C" void *initializeStringListFromNode(void *a)
{
	CNodeStringList *node = new CNodeStringList((CNode*)a);
	return node;
}

extern "C" void *appendNodeToStringList(void *strlist, void *nodeToAdd)
{
	((CNodeStringList*)strlist)->AddItem((CNode*)nodeToAdd);
	return strlist;
}

extern "C" void *createNodeAddition(void *a, void *b)
{
	CNodeAddition *node = new CNodeAddition((CNode *)a, (CNode *)b);
	return node;
}

extern "C" void *createNodeSubtraction(void *a, void *b)
{
	CNodeSubtraction *node = new CNodeSubtraction((CNode *)a, (CNode *)b);
	return node;
}

extern "C" void *createNodeMultiplication(void *a, void *b)
{
	CNodeMultiplication *node = new CNodeMultiplication((CNode *)a, (CNode *)b);
	return node;
}

extern "C" void *createNodeDivision(void *a, void *b)
{
	CNodeDivision *node = new CNodeDivision((CNode *)a, (CNode *)b);
	return node;
}

extern "C" void *createNodePower(void *a, void *b)
{
	CNodePower *node = new CNodePower((CNode *)a, (CNode *)b);
	return node;
}

extern "C" void *createNodeLessThan(void *a, void *b)
{
	CNodeLessThan *node = new CNodeLessThan((CNode *)a, (CNode *)b);
	return node;
}

extern "C" void *createNodeGreaterThan(void *a, void *b)
{
	CNodeGreaterThan *node = new CNodeGreaterThan((CNode *)a, (CNode *)b);
	return node;
}

extern "C" void *createNodeSqrt(void *a)
{
	CNodeSqrt *node = new CNodeSqrt((CNode *)a);
	return node;
}

extern "C" void *createNodeStmts(void *a, void *b)
{
	if(a == NULL)
		return b;

	if(b == NULL)
		return a;

	CNodeStmts *node = new CNodeStmts((CNode *)a, (CNode *)b);
	return node;
}

extern "C" void *createNodeVariable(void *a, void*b)
{
	CNodeVariable *node = new CNodeVariable((CNode *)a, (CNode *)b);
	return node;
}

extern "C" void addToNodeArgumentList(void *a)
{
	//CNodeVariable *node = new CNodeVariable((CNode *)a, (CNode *)b);
	arguments.push_back((CNode *)a);
	//return node;
}

extern "C" void *createNodeFunction(void *a)
{
	CNodeFunction *node = new CNodeFunction((CNode *)a, arguments);
	arguments.clear();
	return node;
}
