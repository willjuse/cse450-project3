%{
/*
 * CSE 450 Project 3
 * Group Members:
 * Matthew Vorce - vorcemat
 * Andrew Matteson - mattes18
 * William Juszczyk - juszczy1
 *
 * Lex file for simplecalc.y
 */

#include <stdio.h>
#include <stdbool.h>
#include "y.tab.h"

int nestlevel = 0;

%}
%s PRINTSTUFF
%%

("mean"|"stdev"|"add")	{char *temp = strdup(yytext); temp[yyleng] = '\0'; yylval.sval = temp; return KEYWORD;}
("help"|"exit"|"clear") {char *temp = strdup(yytext); temp[yyleng] = '\0'; yylval.sval = temp; return SUPPORT;}
","		 {return COMMA; /*printf("Found Comma\n");*/}

"sqrt" {return SQRT;}
"if" {if (nestlevel==0) { clearExecutionList(); } else { /*clear last stack*/ } nestlevel++; return IF;}
"while" {/* maybe wont work with repeated exec?*/ if (nestlevel==0) { clearExecutionList(); } else { /*clear last stack*/ } nestlevel++; return WHILE;}
"{" {return LBRACE;}
"}" {nestlevel--; return RBRACE;}
"print"     {BEGIN(PRINTSTUFF); return PRINT;}

"#".* {return COMMENT;}

[0-9]+    {yylval.dval =  atoi(yytext);  return NUM;}
[0-9]+\.[0-9]+    {yylval.dval = atof(yytext);/*printf("found a floating point");*/ return NUM;}


[a-zA-Z_][a-zA-Z0-9_]* {
				yylval.sval = strdup(yytext);
				double *value;
				//printf("called\n");
				value = malloc(sizeof(double));
				bool flag = CheckVariable(strdup(yytext), value);
				if(flag)
				{
					//yylval.dval = (*value);
					//printf("xzxxxdval: %lf\n", (*value));
					//printf("Matched a variable\n");

					//printf("a: %s\n", strdup(yytext));
					//yylval.pval = createNodeValueFromVar((const char *) strdup(yytext), false /* no negate here */);
					return EXISTINGVAR;
				}
				else
				{
					//yylval.sval = strdup("");
					printf("Error Variable has not been defined\n");
					return ERROR;
					//return EXISTINGVAR;
				}
				free(value);
			 }
				 
[a-zA-Z_][a-zA-Z0-9_]*[ \t]*":=" {
							char *temp1 = strdup(yytext);
							char *temp2 = strdup(yytext);
							int i;
							for(i = 0; i < yyleng; i++)
							{
								if(temp1[i] != ' ' && temp1[i] != '\t' && temp1[i] != ':')
								{
									temp2[i] = temp1[i];
								}
								else
								{
									temp2[i] = '\0';
									break;
								}
							}
							yylval.sval = strdup(temp2);
							return POSNEWVAR;
						 }


<PRINTSTUFF>\"([^\\\"]|\\.)*\"      {yytext[yyleng-1]='\0'; yylval.sval = yytext+1; /*printf("lit: %s\n", yytext+1);*/ return STRTOPRINT;}

[ \t]+    {}
\n        {BEGIN(INITIAL); return '\n';}
\r        {}
\(        {return LPAREN;}
\)        {return RPAREN;}
.         {return yytext[0];}
%%
int  yywrap()
{
	return 1;
}
